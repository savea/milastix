<?php
/*
Plugin Name: MilaStix
Plugin URI: https://milastix.ru/about/
Description: MilaStix functions.
Author: MilaStix
Version: 1.0
Author URI: https://milastix.ru/
*/

/**
 * Register Custom Post Types and Taxonomy.
 */

function custom_post_types() {

	/**
	 * Taxonomy.
	 */

	// Redirects categories
	$labels_redirects_cat = array(
		'all_items'           => 'Категории'
	);
	$args_redirects_post = array(
		'label'               => 'Категории редиректов',
		'rewrite'             => array( 'slug' => 'category-redirect' ),
		'public'              => true,
		'hierarchical'        => true,
		'show_admin_column'   => true,
		'labels'              => $labels_redirects_cat
	);
	register_taxonomy( 'redirects_cat', array( 'redirects' ), $args_redirects_post );

	/**
	 *  Custom Post Types.
	 */

	// Redirects.
	$labels_post_redirects = array(
		'add_new'             => 'Добавить редирект',
		'add_new_item'        => 'Добавление редиректа',
		'edit_item'           => 'Редактировать редирект'
	);
	$args_post_redirects = array(
		'label'               => 'Редиректы',
		'supports'            => array( 'title' ),
		'rewrite'             => array( 'slug' => 'url' ),
		'public'              => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'menu_icon'           => 'dashicons-external',
		'taxonomies'          => array( 'redirects_cat' ),
		'labels'              => $labels_post_redirects
	);
	register_post_type( 'redirects', $args_post_redirects );
}
add_action( 'init', 'custom_post_types', 0 );

/**
 * Register meta box(es).
 */
function redirect_register_meta_boxes() {
	add_meta_box( 'redirects_url', 'URL:', 'redirect_display_callback', 'redirects' );
}
add_action( 'add_meta_boxes', 'redirect_register_meta_boxes' );

/**
 * Render Meta Box content.
 *
 * @param WP_Post $post The post object.
 */
function redirect_display_callback( $post ) {

    // Add an nonce field so we can check for it later.
    wp_nonce_field( 'nonce', 'redirect_nonce' );

    // Use get_post_meta to retrieve an existing value from the database.
    $value = get_post_meta( $post->ID, 'redirect_meta_value_key', true );

    // Display the form, using the current value.
    ?>
    <input type="text" id="redirect_new_field" name="redirect_new_field" value="<?php echo esc_attr( $value ); ?>" style="width:100%" />
    <?php
}

/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function save_metabox_callback( $post_id ) {

	if ( empty( $_POST ) ) {
		return;
	}

	if ( ! isset( $_POST['redirect_nonce'] ) ) {
		return;
	}

	if ( ! wp_verify_nonce( $_POST['redirect_nonce'], 'nonce' ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( isset( $_POST['post_type'] ) && 'redirects' === $_POST['post_type'] ) {
		$my_data = sanitize_text_field( $_POST['redirect_new_field'] );
		update_post_meta( $post_id, 'redirect_meta_value_key', $my_data );
	}

	// Check if $_POST field(s) are available

	// Sanitize

	// Save
}
add_action( 'save_post', 'save_metabox_callback' );

register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, 'myplugin_flush_rewrites' );
function myplugin_flush_rewrites() {
	custom_post_types();
	flush_rewrite_rules();
}
